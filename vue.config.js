module.exports = {
    configureWebpack: {
        target: 'electron-main',
        resolve:{
            mainFields: ['module', 'main']
        }
    }
}