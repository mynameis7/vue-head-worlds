import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Graph from './views/MainApp/Graph.vue';
import MainApp from './views/MainApp/MainApp.vue';
import DetailsPane from './views/MainApp/DetailsPane.vue';
import About from './views/About.vue';
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,

    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/app',
      name: 'app',
      component: MainApp,
      children: [
        {
          path:'vis',
          name: 'visualization',
          component: Graph
        },
        {
          path: 'folders',
          name: 'folders',
          component: DetailsPane
        }
      ]
    },
  ],
});
