import { remote } from 'electron';

export async function open(defaultDirectory?: string, additionalOptions?: Electron.OpenDialogOptions) {
    const window = remote.getCurrentWindow();
    let defaultOptions: Electron.OpenDialogOptions = {
        defaultPath: defaultDirectory || undefined,
    };
    if (additionalOptions) {
        if (defaultDirectory) {
            additionalOptions.defaultPath = defaultDirectory;
        }
        defaultOptions = additionalOptions;
    }
    return new Promise<string[] | undefined>((resolve, reject) => {
        remote.dialog.showOpenDialog(
            window,
            additionalOptions || defaultOptions,
            resolve);
    });
}
