export interface IClickHandlerOptions {
    onClick?: (...args: any[]) => any;
    onDoubleClick?: (...args: any[]) => any;
    doubleClickWait?: number;
}

export class ClickHandler {
    private onClick?: (...args: any[]) => any;
    private onDoubleClick?: (...args: any[]) => any;
    private clickDebounce?: NodeJS.Timeout | number;
    private doubleClickWait: number;
    constructor(options: IClickHandlerOptions) {
        this.doubleClickWait = options.doubleClickWait || 250;
        this.onClick = options.onClick;
        this.onDoubleClick = options.onDoubleClick;
    }
    public onClickHandler(...args: any[]) {
        if (this.onClick && this.clickDebounce == null) {
            this.clickDebounce = setTimeout(() => {
                if (this.onClick) { this.onClick(...args); }
                delete this.clickDebounce;
            }, this.doubleClickWait);
        }
    }

    public onDoubleClickHandler(...args: any[]) {
        this.clear();
        if (this.onDoubleClick) {
            this.onDoubleClick(...args);
        }
    }

    private clear() {
        if (this.clickDebounce != null) { clearTimeout(this.clickDebounce as any); }
        delete this.clickDebounce;
    }

}
