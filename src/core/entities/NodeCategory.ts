import {Entity, Column, PrimaryGeneratedColumn, Index} from 'typeorm';

@Entity()
export default class NodeCategory {
    @PrimaryGeneratedColumn()
    @Index()
    public id!: number;

    @Column('text')
    public name!: string;

    @Column('text')
    public description!: string;
}
