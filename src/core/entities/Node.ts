import {Entity, Column, PrimaryGeneratedColumn, Index, JoinColumn, OneToOne, ManyToOne} from 'typeorm';
import NodeCategory from './NodeCategory';

@Entity()
export default class Node {
    @PrimaryGeneratedColumn()
    @Index()
    public id!: number;

    @Column('text')
    public name!: string;

    @ManyToOne((type) => NodeCategory)
    @Index()
    @JoinColumn()
    public category!: NodeCategory;
}
