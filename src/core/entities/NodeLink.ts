import {Entity, PrimaryGeneratedColumn, Index, JoinColumn, OneToOne} from 'typeorm';
import Node from './Node';

@Entity()
export default class NodeLink {
    @PrimaryGeneratedColumn()
    @Index()
    public id!: number;

    @OneToOne((type) => Node)
    @Index()
    @JoinColumn()
    public primaryNode!: Node;

    @OneToOne((type) => Node)
    @Index()
    @JoinColumn()
    public linkedNode!: Node;
}
