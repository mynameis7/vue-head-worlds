import {getConnection, Connection, Entity as EntityType, createConnection as _createConnection} from 'typeorm/index';
import {
    Node,
    NodeCategory,
    NodeLink,
} from './entities';
const isDevelopment = process.env.NODE_ENV !== 'production';
export async function createConnection(filepath: string) {
    const connectionPath = filepath.split('/');
    const fileName = connectionPath[connectionPath.length - 1];
    const connection = await _createConnection({
        type: 'sqlite',
        synchronize: true,
        logging: isDevelopment,
        logger: 'simple-console',
        database: filepath,
        entities: [Node, NodeCategory, NodeLink],
    });
    return connection;

}

export class Session<T> {
    private connection: Connection;
    private classType: new (...args: any[]) => T;
    constructor(classType: new (...args: any[]) => T) {
        this.connection = getConnection();
        this.classType = classType;
    }

    public async query() {
        return this.connection.getRepository<T>(this.classType);
    }
}
