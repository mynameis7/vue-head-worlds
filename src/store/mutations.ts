import { MutationTree } from 'vuex';
import { RootState } from './';

export const mutations: MutationTree<RootState> = {
    setHeadWorldName(state, payload: {name: string}) {
        state.HeadWorldName = payload.name;
    },
};
