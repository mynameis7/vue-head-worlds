import { ActionTree } from 'vuex';
import { RootState } from '@/store';

export const actions: ActionTree<RootState, RootState> =  {
    setHeadWorldName(state, name) {
        state.rootState.HeadWorldName = name;
    },
    save(state) {
        // do save stuff
        throw new Error('NotImplemented');
    },
};
