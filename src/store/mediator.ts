import { Store } from 'vuex';
import { RootState } from '.';
import VueRouter from 'vue-router';
import {ModuleNames} from './modules';
import {MutationTypes as ElectronMutationTypes} from './modules/Electron/constants';
import {ActionTypes as RecentFilesActionTypes} from './modules/RecentFiles/constants';
import {MutationTypes as DBConnectionManagerMutationtypes} from './modules/DatabaseConnectionManager/constants';
import {ActionTypes as NetworkActionTypes} from './modules/Network/constants';
import {ActionTypes as HeadWorldsMetaDataActionTypes} from './modules/HeadWorldMetaData/constants';
function emit(trigger: (key: string, payload: any) => void, module: string, target: string, payload: any) {
    return trigger(moduleTargetKey(module, target), payload);
}
function moduleTargetKey(module: string, target: string) {
    return `${module}/${target}`;
}
export default function configureModerator(store: Store<RootState>, router: VueRouter) {
    // listen to mutations
    store.subscribe(({ type, payload }, state) => {
      switch (type) {
        case moduleTargetKey(ModuleNames.Electron, ElectronMutationTypes.SET_LAST_OPENED_FILE):
          return emit(
              store.dispatch,
              ModuleNames.RecentFiles,
              RecentFilesActionTypes.UPDATE_RECENT_FILES,
              {newFile: payload.filepath},
          );
        case moduleTargetKey(ModuleNames.DBConnectionManager, DBConnectionManagerMutationtypes.SET_CONNECTION):
          emit(
            store.commit,
            ModuleNames.Electron,
            ElectronMutationTypes.SET_LAST_OPENED_FILE,
            {filepath: payload.connection.options.database},
          );
          emit(
            store.dispatch,
            ModuleNames.Network,
            NetworkActionTypes.LOAD_DATA_FROM_DATABASE,
            {},
          );
          emit(
            store.dispatch,
            ModuleNames.HeadWorldsMetaData,
            HeadWorldsMetaDataActionTypes.LOAD_CATEGORIES_FROM_DATABASE,
            {}
          )
          return router.push('/app/vis');
      }
    });
  }
