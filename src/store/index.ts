import Vue from 'vue';
import Vuex from 'vuex';
import { actions } from './actions';
import { mutations } from './mutations';
import { RootState } from '@/store';
import {Modules as modules} from './modules';
Vue.use(Vuex);

export interface RootState {
  HeadWorldName: string;
}

export default new Vuex.Store<RootState>({
  state() {
    return {
      HeadWorldName: '',
    };
  },
  mutations,
  actions,
  modules
});
