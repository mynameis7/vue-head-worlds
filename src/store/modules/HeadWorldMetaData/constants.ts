export const MutationTypes  = {
    SET_CATEGORIES: 'setCategories'
};

export const ActionTypes = {
    LOAD_CATEGORIES_FROM_DATABASE: 'loadCategoriesFromDatabase',
};

export const GetterTypes = {

};
