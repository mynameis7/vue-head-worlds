import { ActionTree } from 'vuex';
import { HeadWorldMetaDataState } from '@/store/modules/HeadWorldMetaData';
import { RootState } from '@/store';
import { NodeCategory } from '@/core/entities';
import { ActionTypes, MutationTypes} from './constants';
import { Session } from '@/core/Data';
export const actions: ActionTree<HeadWorldMetaDataState, RootState> = {
    async [ActionTypes.LOAD_CATEGORIES_FROM_DATABASE]({commit},{}) {
        let query = await new Session(NodeCategory).query();
        let categories = await query.find();
        commit(MutationTypes.SET_CATEGORIES, { categories });
    }
};
