import {MutationTree} from 'vuex';
import { HeadWorldMetaDataState } from '@/store/modules/HeadWorldMetaData/index';
import {MutationTypes} from './constants';
import {NodeCategory} from '@/core/entities'

export const mutations: MutationTree<HeadWorldMetaDataState> =  {
    [MutationTypes.SET_CATEGORIES](state, {categories}: {categories:NodeCategory[]}) {
        state.nodeCategories = categories;
    }
};
