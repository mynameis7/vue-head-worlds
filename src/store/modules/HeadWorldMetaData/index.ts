import {NodeCategory} from '@/core/entities'
import {Module} from 'vuex';
import { mutations } from './mutations';
import { actions } from './actions';
import {getters} from './getters';
import { RootState } from '@/store';

export interface HeadWorldMetaDataState {
    nodeCategories: NodeCategory[]
}

export const HeadWorldMetaDataModule: Module<HeadWorldMetaDataState, RootState> = {
    namespaced: true,
    state() {
        return {
            nodeCategories: []
        };
    },
    mutations,
    actions,
    getters,
};
