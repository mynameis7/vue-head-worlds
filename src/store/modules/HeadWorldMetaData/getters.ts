import {GetterTree} from 'vuex';
import { HeadWorldMetaDataState } from '@/store/modules/HeadWorldMetaData';
import { RootState } from '@/store';

import {Node, Edge, OptionsShadow} from 'vis';
import { GetterTypes } from './constants';

export const getters: GetterTree<HeadWorldMetaDataState, RootState> = {

};
