import { ActionTree } from 'vuex';
import { ElectronState } from '@/store/modules/Electron';
import { RootState } from '@/store';

import { ActionTypes, MutationTypes} from './constants';
import {ActionTypes as RecentFilesActionTypes} from '@/store/modules/RecentFiles/constants';
import {open} from '@/core/Files';
export const actions: ActionTree<ElectronState, RootState> = {
    async [ActionTypes.OPEN_FILE]({commit}) {
        const properties: Array<'openFile'> = ['openFile'];
        const paths = await open('./', {properties});
        if (paths) {
            const filepath = paths[0];
            commit(MutationTypes.SET_LAST_OPENED_FILE, {filepath});
        } else { return null; }
    },

    async [ActionTypes.OPEN_FOLDER]() {
        const properties: Array<'openDirectory'> = ['openDirectory'];
        const path = await open('./', {properties});
        if (path) {
            return path[0];
        } else { return null; }
    },
};
