
import {Module} from 'vuex';
import { actions } from './actions';
import {mutations} from './mutations';
import { RootState } from '@/store';

export interface ElectronState {
    lastOpenedFile?: string;
}
export const ElectronModule: Module<ElectronState, RootState> = {
    namespaced: true,
    state() {
        return {
        };
    },
    mutations,
    actions,
};
