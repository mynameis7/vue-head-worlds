import {MutationTree} from 'vuex';
import {MutationTypes} from './constants';
import { ElectronState } from '.';
export const mutations: MutationTree<ElectronState> =  {
    async [MutationTypes.SET_LAST_OPENED_FILE](state, {filepath}: {filepath: string} ) {
        state.lastOpenedFile = filepath;
    },

};
