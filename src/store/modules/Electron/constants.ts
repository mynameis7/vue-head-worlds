export const MutationTypes = {
    SET_LAST_OPENED_FILE: 'setLastOpenedFile',
};

export const ActionTypes = {
    OPEN_FILE: 'openFile',
    OPEN_FOLDER: 'openFolder',
};

export const GetterTypes = {
};
