import {NetworkModule} from './Network';
import {DatabaseConnectionManagerModule} from './DatabaseConnectionManager';
import { RecentFilesModule } from './RecentFiles';
import { ElectronModule } from './Electron';
import {HeadWorldMetaDataModule} from './HeadWorldMetaData';
export const ModuleNames = {
    Network: 'Network',
    DBConnectionManager: 'DatabaseConnectionManager',
    Electron: 'Electron',
    RecentFiles: 'RecentFiles',
    HeadWorldsMetaData: 'HeadWorldsMetaData'
};
export const Modules = {
    [ModuleNames.Network]: NetworkModule,
    [ModuleNames.DBConnectionManager]: DatabaseConnectionManagerModule,
    [ModuleNames.RecentFiles]: RecentFilesModule,
    [ModuleNames.Electron]: ElectronModule,
    [ModuleNames.HeadWorldsMetaData]: HeadWorldMetaDataModule
};
