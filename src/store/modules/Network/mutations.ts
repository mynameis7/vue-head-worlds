import {MutationTree} from 'vuex';
import { NetworkState, OrderedPair } from '@/store/modules/Network';
import {MutationTypes} from '../Network/constants';

export const mutations: MutationTree<NetworkState> =  {
    [MutationTypes.SET_NODES](state, payload: {nodes: number[]}) {
        state.nodes = payload.nodes;
    },
    [MutationTypes.SET_EDGES](state, payload: {edges: OrderedPair[]}) {
        state.edges = payload.edges;
    },
    [MutationTypes.ADD_NODE](state, payload: {node: number}) {
        state.nodes.push(payload.node);
    },
    [MutationTypes.ADD_EDGE](state, payload: {edge: OrderedPair}) {
        state.edges.push(payload.edge);
    },
};
