import {GetterTree} from 'vuex';
import { NetworkState, OrderedPair } from '@/store/modules/Network';
import { RootState } from '@/store';

import {Node, Edge, OptionsShadow} from 'vis';
import { GetterTypes } from './constants';

function getShadowOptions(nodeId: number): OptionsShadow {
    return {size: 5, enabled: true, x: 0, y: 0, color: 'grey'};
} 
function getNode(nodeId: number): Node {
    return {id: nodeId, label: nodeId.toString(), shadow: getShadowOptions(nodeId)};
}
function getEdge(pair: OrderedPair): Edge {
    return {from: pair.from, to: pair.to};
}

function nodeInEdgeList(nodeId: number, edgeList: OrderedPair[]) {
    for(let i = 0; i < edgeList.length; i++) {
        if(nodeId === edgeList[i].from) return true;
        if(nodeId === edgeList[i].to) return true;
    }
    return false;
}


export const getters: GetterTree<NetworkState, RootState> = {
    [GetterTypes.NODE_OBJECTS]: (state) => state.nodes ? state.nodes.map((x) => getNode(x)) : [],
    [GetterTypes.EDGE_OBJECTS]: (state) => state.edges ? state.edges.map((x) => getEdge(x)) : [],
    [GetterTypes.SELECTED_NODE_EDGES]: (state) => (state.edges && state.selectedNodeId) ? 
                state.edges
                    .filter(x => x.from === state.selectedNodeId || x.to === state.selectedNodeId)
                    .map(x => getEdge(x)) : [],
    [GetterTypes.SELETED_NODE_AND_NEIGHBORS]: (state, getters) => {
        let edges = getters[GetterTypes.SELECTED_NODE_EDGES]
        return edges.length ? state.nodes.filter(x => nodeInEdgeList(x, edges)).map(x => getNode(x)) : [];
    }
};
