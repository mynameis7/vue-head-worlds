import { ActionTree } from 'vuex';
import { NetworkState, OrderedPair } from '@/store/modules/Network';
import { RootState } from '@/store';
import {Session} from '@/core/Data';
import {Node, NodeLink} from '@/core/entities';

import {MutationTypes, ActionTypes} from '../Network/constants';

function NodeLinkToEdge(nodelink: NodeLink) {
    return {from: nodelink.primaryNode.id, to: nodelink.linkedNode.id};
}

export const actions: ActionTree<NetworkState, RootState> = {
    [ActionTypes.ADD_NODE]({commit}, {nodeId}: {nodeId: number}) {
        commit(MutationTypes.ADD_NODE, {nodeId});
    },
    [ActionTypes.ADD_EDGE]({commit}, {edge}: {edge: OrderedPair}) {
        commit(MutationTypes.ADD_EDGE, {edge});
    },
    [ActionTypes.SET_NODES]({commit}, {nodes}: {nodes: number[]}) {
        commit(MutationTypes.SET_NODES, {nodes});
    },
    [ActionTypes.SET_EDGES]({commit}, {edges}: {edges: OrderedPair[]}) {
        commit(MutationTypes.SET_EDGES, {edges});
    },
    async [ActionTypes.LOAD_DATA_FROM_DATABASE]({commit, rootState}, {}) {
        const [nodeQuery, linkQuery] = await Promise.all([
            new Session(Node).query(),
            new Session(NodeLink).query(),
        ]);
        const [dbNodes, nodeLinks] = await Promise.all([
            nodeQuery.find(),
            linkQuery.find({relations: ['primaryNode', 'linkedNode']}),
        ]);
        const nodes: number[] = dbNodes ? dbNodes.map((x) => x.id) : [];
        const edges: OrderedPair[] =  nodeLinks ? nodeLinks.map(NodeLinkToEdge) : [];
        commit(MutationTypes.SET_NODES, {nodes});
        commit(MutationTypes.SET_EDGES, {edges});
    },
};
