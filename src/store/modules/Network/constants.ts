export const MutationTypes  = {
    ADD_NODE: 'addNode',
    ADD_EDGE: 'addEdge',
    SET_NODES: 'setNodes',
    SET_EDGES: 'setEdges',
};

export const ActionTypes = {
    ADD_NODE: MutationTypes.ADD_NODE,
    ADD_EDGE: MutationTypes.ADD_EDGE,
    SET_NODES: MutationTypes.SET_NODES,
    SET_EDGES: MutationTypes.SET_EDGES,
    LOAD_DATA_FROM_DATABASE: 'loadDataFromDatabase',
};

export const GetterTypes = {
    NODE_OBJECTS: 'nodeObjects',
    EDGE_OBJECTS: 'edgeObjects',
    SELETED_NODE_AND_NEIGHBORS: 'selectedNodeAndNeighbors',
    SELECTED_NODE_EDGES: 'selectedNodeEdges',
};
