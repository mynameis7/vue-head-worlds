
import {Module} from 'vuex';
import { mutations } from './mutations';
import { actions } from './actions';
import {getters} from './getters';
import { RootState } from '@/store';

export interface OrderedPair {
    from: number;
    to: number;
}

export interface NetworkState {
    nodes: number[];
    edges: OrderedPair[];
    selectedNodeId?: number;
}

export const NetworkModule: Module<NetworkState, RootState> = {
    namespaced: true,
    state() {
        return {
            nodes: [],
            edges: [],
        };
    },
    mutations,
    actions,
    getters,
};
