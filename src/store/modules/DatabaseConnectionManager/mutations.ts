import {MutationTree} from 'vuex';
import { DatabaseConnectionManagerState } from '@/store/modules/DatabaseConnectionManager';
import {MutationTypes} from '@/store/modules/DatabaseConnectionManager/constants';
import { Connection } from 'typeorm';
export const mutations: MutationTree<DatabaseConnectionManagerState> =  {
    [MutationTypes.SET_CONNECTION](state, {connection}: {connection: Connection} ) {
        state.connection = connection;
    },

};
