export const MutationTypes = {
    SET_CONNECTION: 'setConnection',
    CREATE_CONNECTION: 'createConnection',
    SET_SESSION_FACTORY: 'setSessionFactory',
};

export const ActionTypes = {
    SET_CONNECTION: MutationTypes.SET_CONNECTION,
    CREATE_CONNECTION: MutationTypes.CREATE_CONNECTION,
};

export const GetterTypes = {
};
