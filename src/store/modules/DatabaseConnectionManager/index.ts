
import {Module} from 'vuex';
import { actions } from './actions';
import { RootState } from '@/store';
import {mutations} from './mutations';
import { Connection } from 'typeorm';

export interface DatabaseConnectionManagerState {
    connection?: Connection;
}
export const DatabaseConnectionManagerModule: Module<DatabaseConnectionManagerState, RootState> = {
    namespaced: true,
    state() {
        return {
        };
    },
    mutations,
    actions,
};
