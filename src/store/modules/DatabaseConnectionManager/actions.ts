import { ActionTree } from 'vuex';
import { RootState } from '@/store';

import { ActionTypes, MutationTypes} from './constants';
import { DatabaseConnectionManagerState } from '.';
import { createConnection } from '@/core/Data';
export const actions: ActionTree<DatabaseConnectionManagerState, RootState> = {
    async [ActionTypes.CREATE_CONNECTION]({commit, state}, {filepath}) {
        if (!state.connection) {
            const connection = await createConnection(filepath);
            commit(MutationTypes.SET_CONNECTION, {connection});
        }

    },
};
