
import {Module} from 'vuex';
import { mutations } from './mutations';
import { actions } from './actions';
import {getters} from './getters';
import { RootState } from '@/store';

export interface RecentFilesState {
    files: string[];
}

export const RecentFilesModule: Module<RecentFilesState, RootState> = {
    namespaced: true,
    state() {
        return {
            files: [],
        };
    },
    mutations,
    actions,
    getters,
};
