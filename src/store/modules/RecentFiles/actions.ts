import { ActionTree } from 'vuex';
import { RecentFilesState } from '@/store/modules/RecentFiles';
import {MutationTypes, ActionTypes} from '../RecentFiles/constants';
import { RootState } from '@/store';

export const actions: ActionTree<RecentFilesState, RootState> = {
    [ActionTypes.LOAD_RECENT_FILES]({commit, state}) {
        if (state.files.length === 0) {
            // load recent files
            let recentFilesString = window.localStorage.getItem('recentFilesList');
            if (!recentFilesString) {
                recentFilesString = '[]';
            }
            window.localStorage.setItem('recentFilesList', recentFilesString);
            let recentFiles;
            try {
                recentFiles = JSON.parse(recentFilesString);
            } catch {
                recentFiles = [];
            }
            // set recent files
            commit(MutationTypes.SET_RECENT_FILES, {recentFiles});
        }

    },
    [ActionTypes.UPDATE_RECENT_FILES]({commit, state}, {newFile}: {newFile: string}) {
        commit(MutationTypes.ADD_NEW_FILE, {newFile});
        window.localStorage.setItem('recentFilesList', JSON.stringify(state.files));
    },
};
