import {MutationTree} from 'vuex';
import { RecentFilesState } from '@/store/modules/RecentFiles';
import {MutationTypes} from '../RecentFiles/constants';

export const mutations: MutationTree<RecentFilesState> =  {
    [MutationTypes.SET_RECENT_FILES](state, {recentFiles}: {recentFiles: string[]} ) {
        state.files = recentFiles;
    },
    [MutationTypes.ADD_NEW_FILE](state, {newFile}: {newFile: string}) {
        const existingFileIndex = state.files.indexOf(newFile);
        if (existingFileIndex > -1) {
            state.files.splice(existingFileIndex, 1);
        }
        state.files.reverse();
        state.files.push(newFile);
        state.files.reverse();
        if (state.files.length > 10) {
            state.files = state.files.slice(0, 10);
        }
    },

};
