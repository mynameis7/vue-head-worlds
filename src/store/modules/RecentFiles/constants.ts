export const MutationTypes = {
    SET_RECENT_FILES: 'setRecentFiles',
    ADD_NEW_FILE: 'addNewFile',
};

export const ActionTypes = {
    UPDATE_RECENT_FILES: 'updateRecentFiles',
    LOAD_RECENT_FILES: 'loadRecentFiles',
};

export const GetterTypes = {
    LAST_3_FILES: 'last3Files',
};
