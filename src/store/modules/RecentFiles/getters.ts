import {GetterTree} from 'vuex';
import { RecentFilesState } from '@/store/modules/RecentFiles';
import { RootState } from '@/store';

import {GetterTypes} from '@/store/modules/RecentFiles/constants';
export const getters: GetterTree<RecentFilesState, RootState> = {
    [GetterTypes.LAST_3_FILES]: (state) => state.files.slice(0, 3),
};
